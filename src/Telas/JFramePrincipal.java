package Telas;

import javax.swing.JMenu;

public class JFramePrincipal extends javax.swing.JFrame {

    private JFrameAutor autor;
    private JFrameCliente cliente;

    public JFramePrincipal() {
        initComponents();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenuCadastroAutor = new javax.swing.JMenu();
        jMenuItem1 = new javax.swing.JMenuItem();
        jMenuCadastrarGenero = new javax.swing.JMenuItem();
        jMenuCadastrarLivro = new javax.swing.JMenuItem();
        jMenuCadastrarCliente = new javax.swing.JMenuItem();
        jMenuPesquisar = new javax.swing.JMenu();
        jMenuPesquisarLivro = new javax.swing.JMenuItem();
        jMenuItemPesquisarCliente = new javax.swing.JMenuItem();
        jMenuEmprestimo = new javax.swing.JMenu();
        jMenuItemNovoEmprestimo = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setLocation(new java.awt.Point(450, 350));

        jPanel1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel1.setFont(new java.awt.Font("Carlito", 3, 48)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 0, 0));
        jLabel1.setText("SysBook");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap(69, Short.MAX_VALUE)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 254, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(49, 49, 49))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        jMenuCadastroAutor.setText("Cadastro");
        jMenuCadastroAutor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuCadastroAutorActionPerformed(evt);
            }
        });

        jMenuItem1.setText("Cadastrar Autor");
        jMenuItem1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jMenuItem1MouseClicked(evt);
            }
        });
        jMenuItem1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem1ActionPerformed(evt);
            }
        });
        jMenuCadastroAutor.add(jMenuItem1);

        jMenuCadastrarGenero.setText("Cadastrar Genero");
        jMenuCadastrarGenero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuCadastrarGeneroActionPerformed(evt);
            }
        });
        jMenuCadastroAutor.add(jMenuCadastrarGenero);

        jMenuCadastrarLivro.setText("Cadastrar Livro");
        jMenuCadastrarLivro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuCadastrarLivroActionPerformed(evt);
            }
        });
        jMenuCadastroAutor.add(jMenuCadastrarLivro);

        jMenuCadastrarCliente.setText("Cadastrar Cliente");
        jMenuCadastrarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuCadastrarClienteActionPerformed(evt);
            }
        });
        jMenuCadastroAutor.add(jMenuCadastrarCliente);

        jMenuBar1.add(jMenuCadastroAutor);

        jMenuPesquisar.setText("Pesquisar");
        jMenuPesquisar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuPesquisarActionPerformed(evt);
            }
        });

        jMenuPesquisarLivro.setText("Livro");
        jMenuPesquisarLivro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuPesquisarLivroActionPerformed(evt);
            }
        });
        jMenuPesquisar.add(jMenuPesquisarLivro);

        jMenuItemPesquisarCliente.setText("Cliente");
        jMenuItemPesquisarCliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemPesquisarClienteActionPerformed(evt);
            }
        });
        jMenuPesquisar.add(jMenuItemPesquisarCliente);

        jMenuBar1.add(jMenuPesquisar);

        jMenuEmprestimo.setText("Emprestimo");
        jMenuEmprestimo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuEmprestimoActionPerformed(evt);
            }
        });

        jMenuItemNovoEmprestimo.setText("Novo");
        jMenuItemNovoEmprestimo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItemNovoEmprestimoActionPerformed(evt);
            }
        });
        jMenuEmprestimo.add(jMenuItemNovoEmprestimo);

        jMenuBar1.add(jMenuEmprestimo);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(91, 91, 91)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(132, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jMenuCadastrarLivroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuCadastrarLivroActionPerformed
        // TODO add your handling code here:
        new JFrameLivro().setVisible(true);
    }//GEN-LAST:event_jMenuCadastrarLivroActionPerformed

    private void jMenuCadastrarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuCadastrarClienteActionPerformed
        // TODO add your handling code here:
        new JFrameCliente().setVisible(true);

    }//GEN-LAST:event_jMenuCadastrarClienteActionPerformed

    private void jMenuItem1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jMenuItem1MouseClicked
        // TODO add your handling code here:


    }//GEN-LAST:event_jMenuItem1MouseClicked

    private void jMenuItem1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem1ActionPerformed
        // TODO add your handling code here:
        new JFrameAutor().setVisible(true);


    }//GEN-LAST:event_jMenuItem1ActionPerformed

    private void jMenuCadastroAutorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuCadastroAutorActionPerformed
        // TODO add your handling code here:

    }//GEN-LAST:event_jMenuCadastroAutorActionPerformed

    private void jMenuCadastrarGeneroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuCadastrarGeneroActionPerformed
        // TODO add your handling code here:
        new JFrameGenero().setVisible(true);
    }//GEN-LAST:event_jMenuCadastrarGeneroActionPerformed

    private void jMenuPesquisarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuPesquisarActionPerformed
        // TODO add your handling code here:

    }//GEN-LAST:event_jMenuPesquisarActionPerformed

    private void jMenuPesquisarLivroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuPesquisarLivroActionPerformed
        // TODO add your handling code here:
        new JFramePesquisaLivro().setVisible(true);
    }//GEN-LAST:event_jMenuPesquisarLivroActionPerformed

    private void jMenuEmprestimoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuEmprestimoActionPerformed
        // TODO add your handling code here:

    }//GEN-LAST:event_jMenuEmprestimoActionPerformed

    private void jMenuItemNovoEmprestimoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemNovoEmprestimoActionPerformed
        // TODO add your handling code here:
        new JFrameEmprestimo().setVisible(true);
    }//GEN-LAST:event_jMenuItemNovoEmprestimoActionPerformed

    private void jMenuItemPesquisarClienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItemPesquisarClienteActionPerformed
        // TODO add your handling code here:
        new JFramePesquisaCliente().setVisible(true);
    }//GEN-LAST:event_jMenuItemPesquisarClienteActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(JFramePrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(JFramePrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(JFramePrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(JFramePrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new JFramePrincipal().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JMenuItem jMenuCadastrarCliente;
    private javax.swing.JMenuItem jMenuCadastrarGenero;
    private javax.swing.JMenuItem jMenuCadastrarLivro;
    private javax.swing.JMenu jMenuCadastroAutor;
    private javax.swing.JMenu jMenuEmprestimo;
    private javax.swing.JMenuItem jMenuItem1;
    private javax.swing.JMenuItem jMenuItemNovoEmprestimo;
    private javax.swing.JMenuItem jMenuItemPesquisarCliente;
    private javax.swing.JMenu jMenuPesquisar;
    private javax.swing.JMenuItem jMenuPesquisarLivro;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
