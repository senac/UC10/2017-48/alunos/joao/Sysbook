package Telas;

import javax.swing.JOptionPane;
import livros.Autor;
import sysbook.SysBook;

public class JFrameAutor extends javax.swing.JFrame {

    private Autor autor;

    public JFrameAutor() {
        initComponents();
    }

    JFrameAutor(JFramePrincipal aThis) {
    }

    private void salvar() {
        String autor = jTextFieldAutorTexto.getText();

        SysBook.addAutor(autor);

        JOptionPane.showMessageDialog(this, "Salvo Com sucesso!", "Sucesso", JOptionPane.INFORMATION_MESSAGE);

        this.dispose();

    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jLabelAutor = new javax.swing.JLabel();
        jTextFieldAutorTexto = new javax.swing.JTextField();
        jButtonSalvarAutor = new javax.swing.JButton();
        jButtonCancelarAutor = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabelAutor.setFont(new java.awt.Font("Arial", 3, 18)); // NOI18N
        jLabelAutor.setText("Autor");

        jTextFieldAutorTexto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTextFieldAutorTextoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(22, 22, 22)
                .addComponent(jLabelAutor, javax.swing.GroupLayout.PREFERRED_SIZE, 59, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(jTextFieldAutorTexto, javax.swing.GroupLayout.PREFERRED_SIZE, 188, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(87, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(30, 30, 30)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabelAutor, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTextFieldAutorTexto, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(33, Short.MAX_VALUE))
        );

        jButtonSalvarAutor.setText("Salvar");
        jButtonSalvarAutor.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButtonSalvarAutorMouseClicked(evt);
            }
        });
        jButtonSalvarAutor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonSalvarAutorActionPerformed(evt);
            }
        });

        jButtonCancelarAutor.setText("Cancelar");
        jButtonCancelarAutor.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jButtonCancelarAutorMouseClicked(evt);
            }
        });
        jButtonCancelarAutor.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButtonCancelarAutorActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addGap(19, 19, 19)
                .addComponent(jButtonSalvarAutor, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(91, 91, 91)
                .addComponent(jButtonCancelarAutor, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButtonSalvarAutor)
                    .addComponent(jButtonCancelarAutor))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTextFieldAutorTextoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTextFieldAutorTextoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTextFieldAutorTextoActionPerformed

    private void jButtonSalvarAutorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonSalvarAutorActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButtonSalvarAutorActionPerformed

    private void jButtonSalvarAutorMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonSalvarAutorMouseClicked
        // TODO add your handling code here:
        this.salvar();

    }//GEN-LAST:event_jButtonSalvarAutorMouseClicked

    private void jButtonCancelarAutorMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jButtonCancelarAutorMouseClicked
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_jButtonCancelarAutorMouseClicked

    private void jButtonCancelarAutorActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButtonCancelarAutorActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jButtonCancelarAutorActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButtonCancelarAutor;
    private javax.swing.JButton jButtonSalvarAutor;
    private javax.swing.JLabel jLabelAutor;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JTextField jTextFieldAutorTexto;
    // End of variables declaration//GEN-END:variables
}
