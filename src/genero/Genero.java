package genero;

public class Genero {

    public String descricao;

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public Genero(String descricao) {
        this.descricao = descricao;
    }

}
